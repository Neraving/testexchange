﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context.Dtos;
using TestExchange.Context.DbModels;

namespace TestExchange.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SaleOrder, SaleOrderDto>().ReverseMap()
                .ForMember("CreationDate", x=>x.MapFrom(c=>DateTime.Now));
            CreateMap<SaleRemainder, SaleOrderDto>()
                .ForMember("Email", x => x.MapFrom(c => c.SaleOrder.Email))
                .ForMember("Price", x => x.MapFrom(c => c.SaleOrder.Price));

            CreateMap<PurchaseOrder, PurchaseOrderDto>().ReverseMap()
                .ForMember("CreationDate", x => x.MapFrom(c => DateTime.Now));
            CreateMap<PurchaseRemainder, PurchaseOrderDto>()
                .ForMember("Email", x => x.MapFrom(c => c.PurchaseOrder.Email))
                .ForMember("Price", x => x.MapFrom(c => c.PurchaseOrder.Price));

            CreateMap<Transaction, TransactionDto>()
                .ForMember("TransactionDate", x => x.MapFrom(c => c.TransactionDate))
                .ForMember("PurchaseDate", x => x.MapFrom(c => c.PurchaseRemider.PurchaseOrder.CreationDate))
                .ForMember("SaleDate", x => x.MapFrom(c => c.SaleReminder.SaleOrder.CreationDate))

                .ForMember("EmailSeller", x => x.MapFrom(c => c.SaleReminder.SaleOrder.Email))
                .ForMember("EmailBuyer", x => x.MapFrom(c => c.PurchaseRemider.PurchaseOrder.Email))

                .ForMember("Amount", x => x.MapFrom(c => c.Amount))
                .ForMember("Price", x => x.MapFrom(c => c.SaleReminder.SaleOrder.Price));


        }
    }
}

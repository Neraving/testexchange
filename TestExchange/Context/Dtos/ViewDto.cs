﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.Dtos
{
    public class ViewDto
    {
        public PurchaseOrderDto[] Orders { get; set; }
        public SaleOrderDto[] Sales { get; set; }
        public TransactionDto[] Transactions { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.Dtos
{
    public class SaleOrderDto
    {
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Email { get; set; }
    }
}

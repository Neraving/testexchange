﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.Dtos
{
    public class TransactionDto
    {
        public DateTime TransactionDate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime SaleDate { get; set; }

        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string EmailSeller { get; set; }
        public string EmailBuyer { get; set; }
    }
}

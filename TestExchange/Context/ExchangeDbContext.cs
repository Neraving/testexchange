﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context.DbModels;

namespace TestExchange.Context
{
    public class ExchangeDbContext : DbContext
    {
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<SaleOrder> SaleOrders { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<SaleRemainder> SaleRemainders { get; set; }
        public DbSet<PurchaseRemainder> PurchaseRemainders { get; set; }

        public ExchangeDbContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PurchaseOrder>().ToTable("PurchaseOrder");
            modelBuilder.Entity<SaleOrder>().ToTable("SaleOrder");
            modelBuilder.Entity<Transaction>().ToTable("Transaction");
            modelBuilder.Entity<SaleRemainder>().ToTable("SaleRemainder");
            modelBuilder.Entity<PurchaseRemainder>().ToTable("PurchaseRemainder");
        }
    }
}

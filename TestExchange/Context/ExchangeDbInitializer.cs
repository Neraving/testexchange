﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context.DbModels;

namespace TestExchange.Context
{
    public class ExchangeDbInitializer
    {
        public ExchangeDbInitializer()
        {
        }


        public IEnumerable<SaleOrder> GetOrders()
        {
            var orders = new List<SaleOrder>();
            var rnd = new Random();

            for (var i = 0; i < 10; i++)
            {
                var amount = rnd.Next(1, 50);
                var price = Convert.ToDecimal(1 + rnd.NextDouble() * 20);
                var date = DateTime.Now;
                orders.Add(new SaleOrder
                {
                    Amount = amount,
                    Price = price,
                    CreationDate = date
                });

            }
            return orders;
        }

        public IEnumerable<SaleRemainder> GetRemainders(IEnumerable<SaleOrder> orders)
        {
            var remainders = new List<SaleRemainder>();
            foreach (var order in orders)
            {
                remainders.Add(new SaleRemainder()
                {
                    Amount = order.Amount,
                    SaleOrder = order,
                    LastUpdate = order.CreationDate,
                });
            }
            return remainders;
        }
    }
}


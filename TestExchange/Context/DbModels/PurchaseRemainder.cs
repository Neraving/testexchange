﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.DbModels
{
    public class PurchaseRemainder
    {
        public Guid Id { get; set; }
        public PurchaseOrder PurchaseOrder { get; set; }
        public int Amount { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool Closed { get; set; }
    }
}

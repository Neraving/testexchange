﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.DbModels
{
    public class SaleOrder
    {
        public Guid Id { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Email { get; set; }
        public DateTime CreationDate { get; set; }
    }
}

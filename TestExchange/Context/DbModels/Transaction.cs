﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestExchange.Context.DbModels
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public PurchaseRemainder PurchaseRemider { get; set; }
        public SaleRemainder SaleReminder { get; set; }
        public int Amount { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}

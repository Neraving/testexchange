﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context;
using TestExchange.Context.DbModels;
using TestExchange.Context.Dtos;

namespace TestExchange.Repository
{
    public partial class SqlRepostitory : IRepository
    {
        private ExchangeDbContext _context;
        private readonly IMapper _mapper;
        public SqlRepostitory(ExchangeDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void CreateSaleOrder(SaleOrderDto item)
        {
            var sale = _mapper.Map<SaleOrder>(item);
            _context.Add(sale);
            
            var purchases = _context.PurchaseRemainders
                .Include(x => x.PurchaseOrder)
                .Where(x => !x.Closed && x.PurchaseOrder.Price <= item.Price)
                .OrderBy(x => x.PurchaseOrder.Price)
                .ToList(); // хотя правльнее в обертку IEnumerable

            var amountLeft = item.Amount;
            int i = 0;
            while (amountLeft > 0 && i < purchases.Count)
            {
                PurchaseRemainder purchaseRemainder = purchases[i];
                var delta = purchaseRemainder.Amount - amountLeft;
                purchaseRemainder.Closed = true;
                purchaseRemainder.LastUpdate = DateTime.Now;
                _context.Update(purchaseRemainder);

                var reminder = new SaleRemainder()
                {
                    Amount = sale.Amount,
                    Closed = true,
                    SaleOrder = sale,
                    LastUpdate = DateTime.Now
                };

                if (delta <= 0)
                {
                    amountLeft -= purchaseRemainder.Amount;
                    reminder.Amount = purchaseRemainder.Amount;
                    i++;
                }
                else
                {
                    var purchaseAmount = new PurchaseRemainder()
                    {
                        Amount = purchaseRemainder.Amount - amountLeft,
                        PurchaseOrder = purchaseRemainder.PurchaseOrder
                    };
                    _context.Add(purchaseAmount);

                    amountLeft = 0;
                    i = purchases.Count();
                }
                _context.Add(reminder);

                var transaction = new Transaction()
                {
                    SaleReminder = reminder,
                    PurchaseRemider = purchaseRemainder,
                    TransactionDate = DateTime.Now,
                    Amount = reminder.Amount
                };

                _context.Add(transaction);
                _context.SaveChanges();
            }

            if (amountLeft > 0)
            {
                var saleReminder = new SaleRemainder
                {
                    Amount = amountLeft,
                    LastUpdate = DateTime.Now,
                    SaleOrder = sale
                };
                _context.Add(saleReminder);
            }

            _context.SaveChanges();
        }

        public void CreatePurchaseOrder(PurchaseOrderDto item)
        {
            var purchase = _mapper.Map<PurchaseOrder>(item);
            _context.Add(purchase);

            var sales = _context.SaleRemainders
                .Include(x => x.SaleOrder)
                .Where(x => !x.Closed && x.SaleOrder.Price <= item.Price)
                .OrderBy(x => x.SaleOrder.Price)
                .ToList(); // хотя правльнее в обертку IEnumerable

            var amountLeft = item.Amount;
            int i = 0;
            while (amountLeft > 0 && i < sales.Count)
            {
                var reminder = new PurchaseRemainder()
                {
                    Amount = amountLeft,
                    Closed = true,
                    PurchaseOrder = purchase,
                    LastUpdate = purchase.CreationDate
                };

                var saleRemainder = sales[i];
                var delta = saleRemainder.Amount - amountLeft;
                saleRemainder.Closed = true;
                saleRemainder.LastUpdate = DateTime.Now;
                _context.Update(saleRemainder);

                if (delta <= 0)
                {
                    amountLeft -= saleRemainder.Amount;
                    reminder.Amount = saleRemainder.Amount;
                    i++;
                }
                else
                {
                    var purchaseAmount = new SaleRemainder()
                    {
                        Amount = saleRemainder.Amount - amountLeft,
                        SaleOrder = saleRemainder.SaleOrder,
                        LastUpdate = DateTime.Now
                    };
                    _context.Add(purchaseAmount);

                    amountLeft = 0;
                    i = sales.Count();
                }
                _context.Add(reminder);

                var transaction = new Transaction()
                {
                    SaleReminder = saleRemainder,
                    PurchaseRemider = reminder,
                    TransactionDate = DateTime.Now,
                    Amount = reminder.Amount
                };

                _context.Add(transaction);
                _context.SaveChanges();
            }

            if (amountLeft > 0)
            {
                var purchaseRemainder = new PurchaseRemainder
                {
                    Amount = amountLeft,
                    LastUpdate = DateTime.Now,
                    PurchaseOrder = purchase
                };
                _context.Add(purchaseRemainder);
            }
            _context.SaveChanges();
        }

        public ViewDto GetData()
        {
            var sales = _context.SaleRemainders
                .Where(x => !x.Closed)
                    .Include(x => x.SaleOrder)
                .OrderBy(x => x.SaleOrder.Price).Take(5).ToList();

            var orders = _context.PurchaseRemainders
                .Where(x => !x.Closed)
                    .Include(x => x.PurchaseOrder)
                .OrderBy(x => x.PurchaseOrder.Price).Take(5).ToList();

            var transactions = _context.Transactions
                .OrderByDescending(x => x.TransactionDate)
                .Take(5)
                    .Include(x => x.PurchaseRemider)
                        .ThenInclude(x => x.PurchaseOrder)
                    .Include(x => x.SaleReminder)
                        .ThenInclude(x => x.SaleOrder)
                .ToList();

            return new ViewDto()
            {
                Sales = _mapper.Map<SaleOrderDto[]>(sales),
                Orders = _mapper.Map<PurchaseOrderDto[]>(orders),
                Transactions = _mapper.Map<TransactionDto[]>(transactions),
            };
        }

        #region Dispose
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

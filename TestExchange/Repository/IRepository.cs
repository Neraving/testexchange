﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context.Dtos;

namespace TestExchange.Repository
{
    public interface IRepository : IDisposable
    {
        ViewDto GetData();
        void CreateSaleOrder(SaleOrderDto item);
        void CreatePurchaseOrder(PurchaseOrderDto item);
    }
}

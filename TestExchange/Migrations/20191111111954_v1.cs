﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestExchange.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PurchaseOrder",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseOrder", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SaleOrder",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleOrder", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseRemainder",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PurchaseOrderId = table.Column<Guid>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Closed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseRemainder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchaseRemainder_PurchaseOrder_PurchaseOrderId",
                        column: x => x.PurchaseOrderId,
                        principalTable: "PurchaseOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SaleRemainder",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SaleOrderId = table.Column<Guid>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Closed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleRemainder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SaleRemainder_SaleOrder_SaleOrderId",
                        column: x => x.SaleOrderId,
                        principalTable: "SaleOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PurchaseRemiderId = table.Column<Guid>(nullable: true),
                    SaleReminderId = table.Column<Guid>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transaction_PurchaseRemainder_PurchaseRemiderId",
                        column: x => x.PurchaseRemiderId,
                        principalTable: "PurchaseRemainder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transaction_SaleRemainder_SaleReminderId",
                        column: x => x.SaleReminderId,
                        principalTable: "SaleRemainder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseRemainder_PurchaseOrderId",
                table: "PurchaseRemainder",
                column: "PurchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleRemainder_SaleOrderId",
                table: "SaleRemainder",
                column: "SaleOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_PurchaseRemiderId",
                table: "Transaction",
                column: "PurchaseRemiderId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_SaleReminderId",
                table: "Transaction",
                column: "SaleReminderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transaction");

            migrationBuilder.DropTable(
                name: "PurchaseRemainder");

            migrationBuilder.DropTable(
                name: "SaleRemainder");

            migrationBuilder.DropTable(
                name: "PurchaseOrder");

            migrationBuilder.DropTable(
                name: "SaleOrder");
        }
    }
}

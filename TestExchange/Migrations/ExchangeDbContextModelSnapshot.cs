﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TestExchange.Context;

namespace TestExchange.Migrations
{
    [DbContext(typeof(ExchangeDbContext))]
    partial class ExchangeDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TestExchange.Context.DbModels.PurchaseOrder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.ToTable("PurchaseOrder");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.PurchaseRemainder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<bool>("Closed")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastUpdate")
                        .HasColumnType("datetime2");

                    b.Property<Guid?>("PurchaseOrderId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("PurchaseOrderId");

                    b.ToTable("PurchaseRemainder");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.SaleOrder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.ToTable("SaleOrder");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.SaleRemainder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<bool>("Closed")
                        .HasColumnType("bit");

                    b.Property<DateTime>("LastUpdate")
                        .HasColumnType("datetime2");

                    b.Property<Guid?>("SaleOrderId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("SaleOrderId");

                    b.ToTable("SaleRemainder");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.Transaction", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<Guid?>("PurchaseRemiderId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid?>("SaleReminderId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("TransactionDate")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("PurchaseRemiderId");

                    b.HasIndex("SaleReminderId");

                    b.ToTable("Transaction");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.PurchaseRemainder", b =>
                {
                    b.HasOne("TestExchange.Context.DbModels.PurchaseOrder", "PurchaseOrder")
                        .WithMany()
                        .HasForeignKey("PurchaseOrderId");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.SaleRemainder", b =>
                {
                    b.HasOne("TestExchange.Context.DbModels.SaleOrder", "SaleOrder")
                        .WithMany()
                        .HasForeignKey("SaleOrderId");
                });

            modelBuilder.Entity("TestExchange.Context.DbModels.Transaction", b =>
                {
                    b.HasOne("TestExchange.Context.DbModels.PurchaseRemainder", "PurchaseRemider")
                        .WithMany()
                        .HasForeignKey("PurchaseRemiderId");

                    b.HasOne("TestExchange.Context.DbModels.SaleRemainder", "SaleReminder")
                        .WithMany()
                        .HasForeignKey("SaleReminderId");
                });
#pragma warning restore 612, 618
        }
    }
}

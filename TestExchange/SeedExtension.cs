﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestExchange.Context;

namespace TestExchange
{
    public static class SeedExtension
    {
        public static IHost Seed(this IHost host)
        {
            using (var scope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (var dbContext = scope.ServiceProvider.GetRequiredService<ExchangeDbContext>())
                {
                    SeedData.SeedAsync(dbContext).GetAwaiter().GetResult();

                    return host;
                }
            }
            return host;
        }

        public static class SeedData
        {
            public static async Task SeedAsync(ExchangeDbContext context)
            {
                if (context.SaleOrders.Any())
                {
                    return;
                }
                var dbSeed = new ExchangeDbInitializer();

                var orders = dbSeed.GetOrders().ToList();
                context.AddRange(orders);
                
                var remainders = dbSeed.GetRemainders(orders).ToList();
                context.AddRange(remainders);
                context.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestExchange.Context.Dtos;
using TestExchange.Repository;

namespace TestExchange.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IRepository _repository;

        public HomeController(ILogger<HomeController> logger, IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public IActionResult Index()
        {
            var data = _repository.GetData();
            ViewBag.Purchases = data.Orders.Reverse();
            ViewBag.Sales = data.Sales.Reverse();
            ViewBag.Transactions = data.Transactions;
            return View();
        }

        [HttpPost]
        public IActionResult AddPurchase(PurchaseOrderDto purchase)
        {
            try
            {
                if (purchase.Amount > 0 && purchase.Price > 0)
                {
                    _repository.CreatePurchaseOrder(purchase);
                }
            }
            catch
            {

            }
            return Redirect("~/");
        }

        [HttpPost]
        public IActionResult AddSale(SaleOrderDto sale)
        {
            try
            {
                if (sale.Amount > 0 && sale.Price > 0)
                {
                    _repository.CreateSaleOrder(sale);
                }
            }
            catch
            {

            }
            return Redirect("~/");
        }
    }
}
